# -*- coding: utf-8 -*-
import datetime

now = datetime.datetime.now()
#store the actual files in the DB and tracks recent changes
db.define_table('file_data',
                Field('file_id', 'integer', requires=IS_NOT_EMPTY()),
                Field('user_id', 'integer', requires=IS_NOT_EMPTY()),
                Field('access_mod', 'integer', requires=IS_NOT_EMPTY()),
                Field('file_blob', 'upload', requires=IS_NOT_EMPTY()),
                Field('prev_date', 'datetime'),
                Field('new_date', 'datetime', default=now))

#tracks all changes made to files
db.define_table('file_change_log',
                Field('file_id', requires=IS_NOT_EMPTY()),
                Field('date_changed', requires=IS_NOT_EMPTY()),
                Field('author', requires=IS_NOT_EMPTY()))

#organize files in projects
db.define_table('project',
                Field('project_id', requires=IS_NOT_EMPTY()),
                Field('project_name', requires=IS_NOT_EMPTY()),
                Field('creator', requires=IS_NOT_EMPTY()),
                Field('access_mod', default='public', requires=IS_NOT_EMPTY()))

#link files and projects
db.define_table('project_files',
                Field('project_id', requires=IS_NOT_EMPTY()),
                Field('file_id', requires=IS_NOT_EMPTY()))

db.define_table('folder',
                Field('folder_id', requires=IS_NOT_EMPTY()),
                Field('parent_id'))
